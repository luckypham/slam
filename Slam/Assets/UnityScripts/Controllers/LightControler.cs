﻿using UnityEngine;

public class LightControler : MonoBehaviour
{
    public GameStateControler GameStateControler;
    public bool isOn = true;
    public int livesLeftTrigger;
    private Animator _animator;

    void Start()
    {
        _animator = GetComponent<Animator>();
        TurnOn();
    }

    void LateUpdate()
    {
        if (isOn && GameStateControler.Lives < livesLeftTrigger) TurnOff();
        if (!isOn && GameStateControler.Lives >= livesLeftTrigger) TurnOn();
    }

    void TurnOn()
    {
        isOn = true;
        _animator.SetBool("zycie", true);
    }

    void TurnOff()
    {
        isOn = false;
        _animator.SetBool("zycie", false);
    }
}
