﻿using UnityEngine;
using System.Collections;

public class PauseControler : MonoBehaviour
{
    public bool isPause = false;
    public SymbolInteractionControler LeftButton;
    public SymbolInteractionControler RightButton;
    public TimeLeftDisplay timer;
    private Animator animator;

    void OnMouseDown()
    {
        if (isPause)
        {
            CancelPause();
            return;
        }
        ApplyPause();
    }

    void ApplyPause()
    {
        timer.StopTimeCount();
        animator.SetBool("pause", true);
    }

    void CancelPause()
    {
        timer.StartTimeCount();
        animator.SetBool("false", true);
    }

}
