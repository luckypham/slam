﻿using Assets.Extensions;
using UnityEngine;

public class SymbolControler : MonoBehaviour
{
    public GameStateControler GameStateControler;

    public SymbolInteractionControler LeftSymbolControler;
    public SymbolInteractionControler RightSymbolControler;
    public SymbolInteractionControler CenterSymbolControler;

    public SymbolDisplay LeftSymbolDisplay;
    public SymbolDisplay RightSymbolDisplay;
    public SymbolDisplay CenterSymbolDisplay;

    public string symbolDefinition;

    void Start()
    {
        GenerateNewSymbols();
    }

    public void SymbolChoiceAction(int value)
    {
        if (IsGoodChoice(value))
        {
            GameStateControler.ScoreUp();
        }
        else
        {
            GameStateControler.LoseLife();
        }
        GenerateNewSymbols();
    }

    public bool IsGoodChoice(int value)
    {
        var pickedSymbolDifference = CenterSymbolControler.GetDifferenceWith(value);
        if (pickedSymbolDifference > CenterSymbolControler.GetDifferenceWith(LeftSymbolControler.SymbolValue) 
            || pickedSymbolDifference > CenterSymbolControler.GetDifferenceWith(RightSymbolControler.SymbolValue))
        {
            return false;
        }
        return true;
    }

    public void GenerateNewSymbols()
    {
        GenerateSymbolDataFor(LeftSymbolControler, LeftSymbolDisplay);
        GenerateSymbolDataFor(RightSymbolControler, RightSymbolDisplay);
        GenerateSymbolDataFor(CenterSymbolControler, CenterSymbolDisplay);
    }

    private void GenerateSymbolDataFor(SymbolInteractionControler controler, SymbolDisplay display)
    {
        var symbol = symbolDefinition.GetRandomLetter();
        controler.UpdateSymbolValue(symbol.GetFirstLetterValue());
        display.DisplaySymbol(symbol);
    }

    public void TurnOff()
    {
        CenterSymbolDisplay.DisplaySymbol("");
        LeftSymbolDisplay.DisplaySymbol("");
        RightSymbolDisplay.DisplaySymbol("");
        LeftSymbolControler.gameObject.SetActive(false);
        RightSymbolControler.gameObject.SetActive(false);
    }
}
