﻿using UnityEngine;
using System.Collections;

public class SymbolDisplay : MonoBehaviour
{
    public TextMesh textMesh;

    public void DisplaySymbol(string newSymbol)
    {
        textMesh.text = newSymbol;
    }

}
