﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RoundDisplay : MonoBehaviour
{
    private TextMesh _scoreText;
    public GameStateControler GameStateControler;

    void Start()
    {
        _scoreText = GetComponentInChildren<TextMesh>();
        _scoreText.text = "1";
    }

    void LateUpdate()
    {
        _scoreText.text = GameStateControler.Round.ToString();
    }
}