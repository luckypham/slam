﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreDisplay : MonoBehaviour
{
    private TextMesh _scoreText;
    public GameStateControler GameStateControler;

    void Start()
    {
        _scoreText = GetComponentInChildren<TextMesh>();
        _scoreText.text = "0";
    }

    void LateUpdate()
    {
        _scoreText.text = GameStateControler.Score.ToString();
    }
}
