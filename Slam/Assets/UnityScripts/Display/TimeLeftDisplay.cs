﻿using UnityEngine;
using System.Collections;

public class TimeLeftDisplay : MonoBehaviour
{
    public GameStateControler GameStateControler;
    public bool isStopped;
    public double timeLeft;
    public int maxNumberOfCharacters = 5;
    private const double offset = 1;
    
    private TextMesh timeView;

    public void SetTimeTo(double seconds)
    {
        timeLeft = seconds;
    }

    public void StopTimeCount()
    {
        isStopped = true;
    }

    public void StartTimeCount()
    {
        isStopped = false;
    }

	void Start ()
	{
	    timeView = GetComponent<TextMesh>();
	    timeLeft = GameStateControler.RoundTime[GameStateControler.Round];
	    isStopped = false;
	}
	
	void Update ()
	{
	    if (timeLeft < 0)
	    {
	        timeLeft = 0;
            GameStateControler.TimeHasRunOut();
	    }

	    if (timeLeft >= 0 && !isStopped)
	    {
	        timeLeft -= Time.deltaTime;
	    }
	    timeView.text = TrimToFullSeconds((timeLeft + offset).ToString());
	}

    string TrimToFullSeconds(string timeString)
    {
        return timeString.Split('.')[0];
    }
}
