﻿using UnityEngine;
using System.Collections;

public class GoToNextScene : MonoBehaviour
{

    public string SceneName;

    void OnMouseDown()
    {
        Application.LoadLevel(SceneName);
    }
}
